# Imagen de ubuntu 20.04
FROM ubuntu:20.04

#Directorio de trabajo 
WORKDIR /src

# Descarga de hadolint dentro del directorio
ADD https://github.com/hadolint/hadolint/releases/download/v2.4.0/hadolint-Linux-x86_64 .

# Se mueve lo descargado al bin y se le da los permisos necesarios
RUN mv ./hadolint-Linux-x86_64 /usr/local/bin/hadolint \
&& chmod +x /usr/local/bin/hadolint

# ENTRYPOINT de ubuntu.
ENTRYPOINT [""]









